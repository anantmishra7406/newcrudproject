package com.RestAPI.test.Repository;

import org.springframework.data.repository.CrudRepository;

import com.RestAPI.test.bean.Employee;

public interface EmployeeRepository  extends CrudRepository<Employee,String> {
	

}
