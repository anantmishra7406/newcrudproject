package com.RestAPI.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BootRestApiCrudAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(BootRestApiCrudAppApplication.class, args);
	}

}
